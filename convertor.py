# This is a manipulation file for kseb data by asishaj
import csv

# write file
poles_output = open('poles_output.csv', 'wb')
trans_output = open('trans_output.csv', 'wb')

poles_writer = csv.writer(poles_output)
trans_writer = csv.writer(trans_output)

poles_writer.writerow(['today', 'username', 'latitude', 'longitude', 'power', 'switch', 'switch_type', 'Type_of_pole', 'Type_of_Structure', 'Number_of_HT_circuits', 'Circuit_mix', 'Line_pole_Cut_pole', 'Whether_muffed_or_not', 'Conductor_type', 'Line_branching_status_of_the_pole_point', 'meta/instanceID'])

trans_writer.writerow(['today', 'username', 'latitude', 'longitude', 'power', 'transformer', 'capacity', 'phase', 'frequency', 'Type_of_Pole', 'Type_of_Structure', 'Number_of_HT_circuits', 'Circuit_mix', 'Line_pole_Cut_pole', 'Whether_muffed_or_not', 'Conductor_type', 'Line_branching_status_of_the_pole_point', 'meta/instanceID'])

with open('input.csv', 'rb') as csvfile:
	spamreader = csv.DictReader(csvfile)
	for row in spamreader:
		poles_row = []
		trans_row = []
		poles_row.extend([row['today'], row['username'], row['_Location_latitude'], row['_Location_longitude'], 'pole'])
		if row['Switching_point_or_not'] != 'na':
			poles_row.extend(['*', row['Switching_point_or_not']])
		else:
			poles_row.extend(['', ''])

		poles_row.extend([row['Type_of_Pole'], row['Type_of_Structure'], row['Number_of_HT_circuits'], row['Circuit_mix'], row['Line_pole_Cut_pole'], row['Whether_muffed_or_not'], row['Conductor_type'], row['Line_branching_status_of_the_pole_point'], row['meta/instanceID']])

		poles_writer.writerow(poles_row)

		if row['Whether_transformer_point_or_n'] == 'yes':
			trans_row.extend([row['today'], row['username'], row['_Location_latitude'], row['_Location_longitude']])
			trans_row.extend(['transformer', 'distribution', row['Transformer_Capacity'], '3', '50Hz'])
			trans_row.extend([row['Type_of_Pole'], row['Type_of_Structure'], row['Number_of_HT_circuits'], row['Circuit_mix'], row['Line_pole_Cut_pole'], row['Whether_muffed_or_not'], row['Conductor_type'], row['Line_branching_status_of_the_pole_point'], row['meta/instanceID']])
			trans_writer.writerow(trans_row)

# close files
poles_output.close()
trans_output.close()
