import csv
import sys

err_log = open('err_log.csv', 'a')
log_writer = csv.writer(err_log)


filename=sys.argv[1]
err=0
total=0
with open(filename,'rb') as file:
    readf=csv.reader(file)
    for row in readf:
        total+=1
        if total==1:
            continue
        if float(row[7]) > 8 :
            err+=1
    print 'Total points: '+str(total)
    print 'Error: '+str(err)
    percent=err*100.0/total
    print 'Err variation: '+str(percent) + '%'
    log_writer.writerow([filename,total,err,percent])

file.close()
err_log.close()
